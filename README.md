# SupermercadoSancor

API REST de los carritos virtuales del supermercado y sus productos.

Tecnologias: Java 8- Spring boot - JPA

endpoing demo:

obtener los carritos

http://localhost:8080/api/v1/carritos

url base de datos

http://localhost:8080/h2-console

acceso

driver class: org.h2.Driver
jdbc url: jdbc:h2:mem:testdb
userName: sa
password: password

Swagger 2

http://localhost:8080/swagger-ui.html#/
