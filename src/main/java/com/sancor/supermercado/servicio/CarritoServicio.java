package com.sancor.supermercado.servicio;

import com.sancor.supermercado.dto.*;
import com.sancor.supermercado.entidad.Carrito;
import com.sancor.supermercado.entidad.Producto;
import com.sancor.supermercado.excepcion.NoEncontroExcepcion;
import com.sancor.supermercado.repositorio.CarritoRepositorio;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarritoServicio {

    private final CarritoRepositorio carritoRepositorio;

    @Autowired
    public CarritoServicio(CarritoRepositorio carritoRepositorio) {
        this.carritoRepositorio = carritoRepositorio;
    }

    public List<CarritoDto> obtenerTodosCarritos() {
        List<Carrito> listaCarrito = carritoRepositorio.findAll();
        return mapearListaCarritoAListaCarritoDto(listaCarrito);
    }

    private List<CarritoDto> mapearListaCarritoAListaCarritoDto(List<Carrito> listaCarrito) {
        List<CarritoDto> listaCarritoDto = new ArrayList<>();
        for (Carrito carrito : listaCarrito) {
            CarritoDto carritoDto = mapearCarritoACarritoDto(carrito);
            listaCarritoDto.add(carritoDto);
        }
        return listaCarritoDto;
    }

    public CarritoDto obtenerCarrito(Integer id) throws NoEncontroExcepcion {
        Optional<Carrito> carritoOptional = carritoRepositorio.findById(id);
        if (!carritoOptional.isPresent()) {
            throw new NoEncontroExcepcion("No encontro el carrito");
        }
        Carrito carrito = carritoOptional.get();
        return mapearCarritoACarritoDto(carrito);
    }

    private CarritoDto mapearCarritoACarritoDto(Carrito carrito) {
        CarritoDto carritoDto = new ModelMapper().map(carrito, CarritoDto.class);
        if (!carrito.getProductos().isEmpty()) {
            List<Producto> lista = carrito.getProductos();
            List<ProductoDto> listaDto = new ArrayList<>();
            for (Producto producto : lista) {
                ProductoDto productoDto = new ModelMapper().map(producto, ProductoDto.class);
                listaDto.add(productoDto);
            }
            carritoDto.setProductos(listaDto);
        } else {
            List<ProductoDto> listaDto = new ArrayList<>();
            carritoDto.setProductos(listaDto);
        }
        return carritoDto;
    }

    public Carrito crearCarrito(CrearCarritoDto crearCarritoDto) {
        Carrito carrito = mapearCrearCarritoDtoACarrito(crearCarritoDto);
        return carritoRepositorio.save(carrito);
    }

    private Carrito mapearCrearCarritoDtoACarrito(CrearCarritoDto crearCarritoDto) {
        Carrito carrito = new ModelMapper().map(crearCarritoDto, Carrito.class);
        if (!carrito.getProductos().isEmpty()) {
            List<CrearProductoDto> listaDto = crearCarritoDto.getProductos();
            List<Producto> lista = new ArrayList<>();
            for (CrearProductoDto crearProductoDto : listaDto) {
                Producto producto = new ModelMapper().map(crearProductoDto, Producto.class);
                lista.add(producto);
            }
            carrito.setProductos(lista);
        } else {
            List<Producto> lista = new ArrayList<>();
            carrito.setProductos(lista);
        }
        return carrito;
    }

    public Carrito actualizarCarrito(Integer id, ActualizarCarritoDto actualizarCarritoDto) throws NoEncontroExcepcion {
        Optional<Carrito> carritoOptional = carritoRepositorio.findById(id);
        if (!carritoOptional.isPresent()) {
            throw new NoEncontroExcepcion("No encontro el carrito");
        }
        Carrito carrito = actualizoCarritoDtoACarrito(carritoOptional.get(), actualizarCarritoDto);
        return carritoRepositorio.save(carrito);
    }

    private Carrito actualizoCarritoDtoACarrito(Carrito carrito, ActualizarCarritoDto actualizarCarritoDto) {
        carrito.setNombreUsuarioCarrito(actualizarCarritoDto.getNombreUsuarioCarrito());
        carrito.setCantidadProductos(actualizarCarritoDto.getCantidadProductos());
        carrito.setProductos(mapearListaCrearProductoDtoAListaProducto(actualizarCarritoDto.getProductos()));
        return carrito;
    }

    private List<Producto> mapearListaCrearProductoDtoAListaProducto(List<CrearProductoDto> listaProductoDto) {
        List<Producto> listaProducto = new ArrayList<>();
        for (CrearProductoDto crearProductoDto : listaProductoDto) {
            Producto producto = new ModelMapper().map(crearProductoDto, Producto.class);
            listaProducto.add(producto);
        }
        return listaProducto;
    }

    public Carrito eliminarCarrito(Integer id) throws NoEncontroExcepcion {
        Optional<Carrito> carritoOptional = carritoRepositorio.findById(id);
        if (!carritoOptional.isPresent()) {
            throw new NoEncontroExcepcion("No encontro el carrito");
        }
        carritoRepositorio.deleteById(id);
        return carritoOptional.get();
    }
}
