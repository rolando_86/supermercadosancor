package com.sancor.supermercado.servicio;

import com.sancor.supermercado.entidad.Producto;
import com.sancor.supermercado.repositorio.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductoServicio {

    private final ProductoRepositorio productoRepositorio;

    @Autowired
    public ProductoServicio(ProductoRepositorio productoRepositorio) {
        this.productoRepositorio = productoRepositorio;
    }

    public List<Producto> obtenerProductos() {
        return productoRepositorio.findAll();
    }
}
