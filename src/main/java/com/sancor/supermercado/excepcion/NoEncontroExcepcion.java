package com.sancor.supermercado.excepcion;

public class NoEncontroExcepcion extends Exception {

    public NoEncontroExcepcion(String mensaje) {
        super(mensaje);
    }
}
