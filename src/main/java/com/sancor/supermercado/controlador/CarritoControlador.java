package com.sancor.supermercado.controlador;

import com.sancor.supermercado.dto.ActualizarCarritoDto;
import com.sancor.supermercado.dto.CrearCarritoDto;
import com.sancor.supermercado.excepcion.NoEncontroExcepcion;
import com.sancor.supermercado.servicio.CarritoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/carrito")
public class CarritoControlador {

    private final CarritoServicio carritoServicio;

    @Autowired
    public CarritoControlador(CarritoServicio carritoServicio) {
        this.carritoServicio = carritoServicio;
    }

    @GetMapping
    public ResponseEntity<?> obtenerTodosCarritos() {
        return ResponseEntity.ok(carritoServicio.obtenerTodosCarritos());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> obtenerCarrito(@PathVariable(value = "id") Integer id) throws NoEncontroExcepcion {
        return ResponseEntity.ok(carritoServicio.obtenerCarrito(id));
    }

    @PostMapping
    public ResponseEntity<?> crearCarrito(@Valid @RequestBody CrearCarritoDto crearCarritoDto) {
        return ResponseEntity.ok(carritoServicio.crearCarrito(crearCarritoDto));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<?> actualizarCarrito(@PathVariable(value = "id") Integer id, @Valid @RequestBody ActualizarCarritoDto actualizarCarritoDto) throws NoEncontroExcepcion {
        return ResponseEntity.ok(carritoServicio.actualizarCarrito(id, actualizarCarritoDto));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> eliminarCarrito(@PathVariable(value = "id") Integer id) throws NoEncontroExcepcion {
        return ResponseEntity.ok(carritoServicio.eliminarCarrito(id));
    }
}
