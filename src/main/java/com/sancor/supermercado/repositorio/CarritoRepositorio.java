package com.sancor.supermercado.repositorio;

import com.sancor.supermercado.entidad.Carrito;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarritoRepositorio extends JpaRepository<Carrito,Integer> {

}
