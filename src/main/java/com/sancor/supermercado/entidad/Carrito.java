package com.sancor.supermercado.entidad;

import javax.persistence.*;
import java.util.List;

@Entity
public class Carrito {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "nombre_usuario_carrito")
    private String nombreUsuarioCarrito;

    @Column(name = "cantidad_productos")
    private Integer cantidadProductos;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_carrito")
    private List<Producto> productos;

    public Carrito() {
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreUsuarioCarrito() {
        return nombreUsuarioCarrito;
    }

    public void setNombreUsuarioCarrito(String nombreUsuarioCarrito) {
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
    }

    public Integer getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(Integer cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }
}
