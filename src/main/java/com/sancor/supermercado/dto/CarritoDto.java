package com.sancor.supermercado.dto;

import java.util.List;

public class CarritoDto {

    private Integer id;
    private String nombreUsuarioCarrito;
    private Integer cantidadProductos;
    private List<ProductoDto> productos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreUsuarioCarrito() {
        return nombreUsuarioCarrito;
    }

    public void setNombreUsuarioCarrito(String nombreUsuarioCarrito) {
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
    }

    public Integer getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(Integer cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }

    public List<ProductoDto> getProductos() {
        return productos;
    }

    public void setProductos(List<ProductoDto> productos) {
        this.productos = productos;
    }
}
