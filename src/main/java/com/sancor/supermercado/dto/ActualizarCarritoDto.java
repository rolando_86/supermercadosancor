package com.sancor.supermercado.dto;

import java.util.List;

public class ActualizarCarritoDto {

    private String nombreUsuarioCarrito;
    private Integer cantidadProductos;
    private List<CrearProductoDto> productos;

    public String getNombreUsuarioCarrito() {
        return nombreUsuarioCarrito;
    }

    public void setNombreUsuarioCarrito(String nombreUsuarioCarrito) {
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
    }

    public Integer getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(Integer cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }

    public List<CrearProductoDto> getProductos() {
        return productos;
    }

    public void setProductos(List<CrearProductoDto> productos) {
        this.productos = productos;
    }
}
