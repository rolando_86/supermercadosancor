DROP TABLE IF EXISTS carrito;
DROP TABLE IF EXISTS carrito_producto;
DROP TABLE IF EXISTS producto;

CREATE TABLE carrito (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nombre_usuario_carrito VARCHAR(250) NOT NULL,
  cantidad_productos INT NOT NULL,
);

INSERT INTO carrito (nombre_usuario_carrito, cantidad_productos) VALUES
  ('CarritoUno', 10),
  ('CarritoDos', 20);

CREATE TABLE producto (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  codigo INT NOT NULL,
  nombre VARCHAR(250) NOT NULL,
  precio INT NOT NULL,
);


