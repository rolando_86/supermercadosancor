package com.sancor.supermercado;

import com.sancor.supermercado.dto.*;
import com.sancor.supermercado.entidad.Carrito;
import com.sancor.supermercado.entidad.Producto;

import java.util.ArrayList;
import java.util.List;

public class TestDataBuilder {

    public static Carrito crearUnCarrito(Integer id, String nombreUsuario) {

        List<Producto> listaProductos = new ArrayList<>();
        listaProductos.add(crearUnProducto(1));
        listaProductos.add(crearUnProducto(2));
        Carrito carrito = new Carrito();
        carrito.setId(id);
        carrito.setCantidadProductos(10);
        carrito.setNombreUsuarioCarrito(nombreUsuario);
        carrito.setProductos(listaProductos);
        return carrito;
    }

    public static Carrito crearUnCarritoSinProductos(Integer id, String nombreUsuario) {
        Carrito carrito = new Carrito();
        carrito.setId(id);
        carrito.setCantidadProductos(10);
        carrito.setNombreUsuarioCarrito(nombreUsuario);
        carrito.setProductos(new ArrayList<>());
        return carrito;
    }

    public static CarritoDto crearCarritoDto(Integer id, String nombreUsuario) {
        CarritoDto carritoDto = new CarritoDto();
        carritoDto.setId(1);
        carritoDto.setCantidadProductos(10);
        carritoDto.setNombreUsuarioCarrito("carlitos");
        carritoDto.setProductos(new ArrayList<>());
        return carritoDto;
    }

    public static Producto crearUnProducto(Integer id) {
        Producto producto = new Producto();
        producto.setId(id);
        producto.setCodigo(5788);
        producto.setIdCarrito(1);
        producto.setNombre("Caja de Herramientas - " + id);
        producto.setPrecio(300);
        return producto;
    }

    public static ProductoDto crearUnProductoDto(Integer id) {
        ProductoDto productoDto = new ProductoDto();
        productoDto.setId(id);
        productoDto.setCodigo(5788);
        productoDto.setNombre("Caja de Herramientas - " + id);
        productoDto.setPrecio(300);
        return productoDto;
    }

    public static CrearProductoDto crearProductoDtoClass(Integer id) {
        CrearProductoDto crearProductoDto = new CrearProductoDto();
        crearProductoDto.setCodigo(5788);
        crearProductoDto.setNombre("Caja de Herramientas - " + id);
        crearProductoDto.setPrecio(300);
        return crearProductoDto;
    }

    public static CrearCarritoDto crearUnCarritoDtoClass(String nombreUsuario) {
        List<CrearProductoDto> listaCrearProductoDto = new ArrayList<>();
        listaCrearProductoDto.add(crearProductoDtoClass(1));
        listaCrearProductoDto.add(crearProductoDtoClass(2));
        CrearCarritoDto crearCarritoDto = new CrearCarritoDto();
        crearCarritoDto.setNombreUsuarioCarrito(nombreUsuario);
        crearCarritoDto.setCantidadProductos(10);
        crearCarritoDto.setProductos(listaCrearProductoDto);
        return crearCarritoDto;
    }

    public static CrearCarritoDto crearUnCarritoDtoClassSinProductos(String nombreUsuario) {
        CrearCarritoDto crearCarritoDto = new CrearCarritoDto();
        crearCarritoDto.setNombreUsuarioCarrito(nombreUsuario);
        crearCarritoDto.setCantidadProductos(10);
        crearCarritoDto.setProductos(new ArrayList<>());
        return crearCarritoDto;
    }

    public static ActualizarCarritoDto actualizarCarritoDtoClass(String nombreUsuario) {
        List<CrearProductoDto> listaCrearProductoDto = new ArrayList<>();
        listaCrearProductoDto.add(crearProductoDtoClass(1));
        listaCrearProductoDto.add(crearProductoDtoClass(2));
        ActualizarCarritoDto actualizarCarritoDto = new ActualizarCarritoDto();
        actualizarCarritoDto.setNombreUsuarioCarrito(nombreUsuario);
        actualizarCarritoDto.setCantidadProductos(10);
        actualizarCarritoDto.setProductos(listaCrearProductoDto);
        return actualizarCarritoDto;
    }
}
