package com.sancor.supermercado.servicio;

import com.sancor.supermercado.dto.CarritoDto;
import com.sancor.supermercado.entidad.Carrito;
import com.sancor.supermercado.excepcion.NoEncontroExcepcion;
import com.sancor.supermercado.repositorio.CarritoRepositorio;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.sancor.supermercado.TestDataBuilder.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CarritoServicioTest {

    @MockBean
    public CarritoRepositorio carritoRepositorio;

    public CarritoServicio carritoServicio;

    @Before
    public void setUp() {
        carritoServicio = new CarritoServicio(carritoRepositorio);
        List<Carrito> listaCarrito = new ArrayList<>();
        listaCarrito.add(crearUnCarrito(1, "carlitos"));
        when(carritoRepositorio.findAll()).thenReturn(listaCarrito);
        Optional<Carrito> optionalCarrito = Optional.ofNullable(crearUnCarrito(1, "carlitos"));
        when(carritoRepositorio.findById(any(Integer.class))).thenReturn(optionalCarrito);
    }

    @Test
    public void obtenerTodosLosCarritos() {
        List<CarritoDto> listaCarritos = carritoServicio.obtenerTodosCarritos();
        assertEquals(1, listaCarritos.size());
        assertEquals("carlitos", listaCarritos.get(0).getNombreUsuarioCarrito());
    }

    @Test
    public void obtenerCarrito() throws NoEncontroExcepcion {
        CarritoDto carritoDto = carritoServicio.obtenerCarrito(1);
        assertEquals("carlitos", carritoDto.getNombreUsuarioCarrito());
    }

    @Test
    public void obtenerCarritoSinProductos() throws NoEncontroExcepcion {
        Optional<Carrito> optionalCarrito = Optional.ofNullable(crearUnCarritoSinProductos(1, "carlitos"));
        when(carritoRepositorio.findById(any(Integer.class))).thenReturn(optionalCarrito);
        CarritoDto carritoDto = carritoServicio.obtenerCarrito(1);
        assertEquals("carlitos", carritoDto.getNombreUsuarioCarrito());
    }

    @Test
    public void crearCarrito() {
        when(carritoRepositorio.save(any(Carrito.class))).thenReturn(crearUnCarrito(1, "carlitos"));
        Carrito carritoService = carritoServicio.crearCarrito(crearUnCarritoDtoClass("carlitos"));
        assertEquals("carlitos", carritoService.getNombreUsuarioCarrito());
    }

    @Test
    public void crearCarritoSinProductos() {
        when(carritoRepositorio.save(any(Carrito.class))).thenReturn(crearUnCarritoSinProductos(1, "carlitos"));
        Carrito carritoService = carritoServicio.crearCarrito(crearUnCarritoDtoClassSinProductos("carlitos"));
        assertEquals("carlitos", carritoService.getNombreUsuarioCarrito());
    }

    @Test
    public void actualizarCarrito() throws NoEncontroExcepcion {
        when(carritoRepositorio.save(any(Carrito.class))).thenReturn(crearUnCarrito(1, "carlos"));
        Carrito carritoService = carritoServicio.actualizarCarrito(1, actualizarCarritoDtoClass("carlos"));
        assertEquals("carlos", carritoService.getNombreUsuarioCarrito());
    }

    @Test
    public void eliminarCarrito() throws NoEncontroExcepcion {
        Optional<Carrito> optionalCarrito = Optional.ofNullable(crearUnCarrito(1, "carlos"));
        when(carritoRepositorio.findById(any(Integer.class))).thenReturn(optionalCarrito);
        Carrito carritoService = carritoServicio.eliminarCarrito(2);
        assertEquals("carlos", carritoService.getNombreUsuarioCarrito());
    }

    @Test(expected = NoEncontroExcepcion.class)
    public void obtenerCarritoException() throws NoEncontroExcepcion {
        Optional<Carrito> optionalCarrito = Optional.ofNullable(null);
        when(carritoRepositorio.findById(any(Integer.class))).thenReturn(optionalCarrito);
        CarritoDto carritoDto = carritoServicio.obtenerCarrito(10);
    }

    @Test(expected = NoEncontroExcepcion.class)
    public void actualizarCarritoException() throws NoEncontroExcepcion {
        when(carritoRepositorio.findById(any(Integer.class))).thenReturn(Optional.ofNullable(null));
        when(carritoRepositorio.save(any(Carrito.class))).thenReturn(crearUnCarrito(1, "carlitos"));
        Carrito carritoService = carritoServicio.actualizarCarrito(5, actualizarCarritoDtoClass("carlos"));
    }

    @Test(expected = NoEncontroExcepcion.class)
    public void eliminarCarritoException() throws NoEncontroExcepcion {
        when(carritoRepositorio.findById(any(Integer.class))).thenReturn(Optional.ofNullable(null));
        Carrito carritoService = carritoServicio.eliminarCarrito(2);
    }
}