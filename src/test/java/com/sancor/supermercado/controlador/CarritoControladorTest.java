package com.sancor.supermercado.controlador;

import com.sancor.supermercado.dto.ActualizarCarritoDto;
import com.sancor.supermercado.dto.CarritoDto;
import com.sancor.supermercado.dto.CrearCarritoDto;
import com.sancor.supermercado.entidad.Carrito;
import com.sancor.supermercado.excepcion.NoEncontroExcepcion;
import com.sancor.supermercado.servicio.CarritoServicio;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static com.sancor.supermercado.TestDataBuilder.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CarritoControladorTest {

    @MockBean
    public CarritoServicio carritoServicio;

    public CarritoControlador carritoControlador;

    @Before
    public void setUp() throws NoEncontroExcepcion {
        carritoControlador = new CarritoControlador(carritoServicio);

        List<CarritoDto> listaCarritos = new ArrayList<>();
        listaCarritos.add(crearCarritoDto(1, "carlitos"));
        when(carritoServicio.obtenerCarrito(any(Integer.class))).thenReturn(crearCarritoDto(1, "carlitos"));
        when(carritoServicio.obtenerTodosCarritos()).thenReturn(listaCarritos);

        when(carritoServicio.crearCarrito(any(CrearCarritoDto.class))).thenReturn(crearUnCarrito(1, "carlitos"));

        when(carritoServicio.actualizarCarrito(any(Integer.class), any(ActualizarCarritoDto.class))).thenReturn(crearUnCarrito(1, "carlos"));
        when(carritoServicio.eliminarCarrito(any(Integer.class))).thenReturn(crearUnCarrito(1, "carlitos"));

    }

    @Test
    public void obtenerCarritoPorId() throws NoEncontroExcepcion {
        ResponseEntity<?> response = carritoControlador.obtenerCarrito(1);
        CarritoDto carritoDto = (CarritoDto) response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("carlitos", carritoDto.getNombreUsuarioCarrito());
    }

    @Test
    public void obtenerCarritos() {
        ResponseEntity<?> response = carritoControlador.obtenerTodosCarritos();
        List<CarritoDto> listaCarritos = (List<CarritoDto>) response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, listaCarritos.size());
        CarritoDto carritoAux = listaCarritos.get(0);
        assertEquals("carlitos", carritoAux.getNombreUsuarioCarrito());
    }

    @Test
    public void agregarCarrito() {
        ResponseEntity<?> response = carritoControlador.crearCarrito(crearUnCarritoDtoClass("carlitos"));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Carrito carrito = (Carrito) response.getBody();
        assertEquals("carlitos", carrito.getNombreUsuarioCarrito());
    }

    @Test
    public void actualizarCarrito() throws NoEncontroExcepcion {
        ResponseEntity<?> response = carritoControlador.actualizarCarrito(1, actualizarCarritoDtoClass("carlos"));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Carrito carrito = (Carrito) response.getBody();
        assertEquals("carlos", carrito.getNombreUsuarioCarrito());
    }

    @Test
    public void eliminarCarrito() throws NoEncontroExcepcion {
        ResponseEntity<?> response = carritoControlador.eliminarCarrito(1);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}